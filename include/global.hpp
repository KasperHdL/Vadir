#pragma once
#include <hincl.hpp>

//forward declarations
typedef struct ILogger    ILogger;
typedef struct IAllocator IAllocator;


namespace Global
{
extern ILogger*    logger;
extern IAllocator* allocator;
extern IAllocator* temp_allocator;

void init_with_defaults();
void init(ILogger* logger, IAllocator* allocator, IAllocator* temp_allocator);

void set_logger(u64 logger_hash);
void set_allocator(u64 allocator_hash);
void set_temp_allocator(u64 allocator_hash);
}; // namespace Global

#if DEBUG

#    define LOG(x) Global::logger->log(__FILE__, __func__, __LINE__, x "\n")
#    define LOGF(x, ...) Global::logger->log(__FILE__, __func__, __LINE__, x "\n", __VA_ARGS__)

#    define WARNING(x) Global::logger->warning(__FILE__, __func__, __LINE__, x "\n")
#    define WARNINGF(x, ...) Global::logger->warning(__FILE__, __func__, __LINE__, x "\n", __VA_ARGS__)

#    define ERROR(x) Global::logger->error(__FILE__, __func__, __LINE__, x "\n")
#    define ERRORF(x, ...) Global::logger->error(__FILE__, __func__, __LINE__, x "\n", __VA_ARGS__)

#    define ASSERT(b) \
        if (!(b)) { (*Global::logger->assert)(#b, __FILE__, __func__, __LINE__, "\n"); }
#    define ASSERTM(b, x) \
        if (!(b)) { (*Global::logger->assert)(#b, __FILE__, __func__, __LINE__, x "\n"); }
#    define ASSERTF(b, x, ...) \
        if (!(b)) { (*Global::logger->assert)(#b, __FILE__, __func__, __LINE__, x "\n", __VA_ARGS__); }

#else

#    define LOG(x)
#    define LOGF(x, ...)

#    define WARNING(x)
#    define WARNINGF(x, ...)

#    define ERROR(x)
#    define ERRORF(x, ...)

#    define ASSERT(b, x)
#    define ASSERTF(b, x, ...)

#endif
