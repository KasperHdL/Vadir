#pragma once
#include <inttypes.h>
#include <stdbool.h>

///@file std_types.h
///Defines better names for base types, so that they precisely specify size.

///@name Types
/// @{
///Defines better names for base types, so that they precisely specify size.
typedef int8_t   s8;
typedef int16_t  s16;
typedef int32_t  s32;
typedef int64_t  s64;

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef u8       byte;
typedef u32      uint;

typedef float    r32;
typedef double   r64;

///@}
