#pragma once
#include <hincl.hpp>

typedef struct IAllocator{
    void* (*allocate)(IAllocator* allocator, u32 size);
    void (*deallocate)(IAllocator* allocator, void* pointer);

    u32 (*allocated_size)(IAllocator* allocator, void* pointer);
    u32 (*total_allocated)(IAllocator* allocator);
} IAllocator;

#define ALLOCATE(allocator, size)           allocator->allocate(       allocator, size)
#define DEALLOCATE(allocator, pointer)      allocator->deallocate(     allocator, pointer)
#define ALLOCATED_SIZE(allocator, pointer)  allocator->allocated_size( allocator, pointer)
#define TOTAL_ALLOCATED(allocator)          allocator->total_allocated(allocator)

