#pragma once
#include <hincl.hpp>

#ifndef RING_BUFFER_SIZE
#define RING_BUFFER_SIZE 4096
#endif

typedef struct IAllocator IAllocator;

void* _ring_allocate(IAllocator* allocator, u32 size);
void  _ring_deallocate(IAllocator* allocator, void* pointer);

u32 _ring_allocated_size(IAllocator* allocator, void* pointer);
u32 _ring_total_allocated(IAllocator* allocator);


IAllocator* get_ring_allocator();


