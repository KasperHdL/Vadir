#pragma once
#include <hincl.hpp>

template <class T> class Array{
public:
    uint capacity;
    uint count;

    T* buffer;

    void allocate(uint capacity);
    void reallocate(uint capacity);
    void deallocate();

    void add(T value);
    void set(uint index, T value);

    T get_copy(uint index);
    T* get_pointer(uint index);

    T& operator[](uint index);
    Array<T>& operator=(const Array<T>& other);
};
