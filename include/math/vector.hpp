#pragma once
#include <hincl.hpp>

//
#include <stdio.h>

struct v3
{
    float x,y,z;

    void operator+=(v3 other){
        x += other.x;
        y += other.y;
        z += other.z;
    }
    void operator-=(v3 other){
        x -= other.x;
        y -= other.y;
        z -= other.z;
    }
    void operator*=(v3 other){
        x *= other.x;
        y *= other.y;
        z *= other.z;
    }
    void operator/=(v3 other){
        x /= other.x;
        y /= other.y;
        z /= other.z;
    }
    void operator*=(float scalar){
        x *= scalar;
        y *= scalar;
        z *= scalar;
    }
    void operator/=(float scalar){
        x /= scalar;
        y /= scalar;
        z /= scalar;
    }


    v3 operator+(v3 other){
        v3 n = 
        {
            x + other.x,
            y + other.y,
            z + other.z
        };
        return n;
    }

    v3 operator-(v3 other){
        v3 n = 
        {
            x - other.x,
            y - other.y,
            z - other.z
        };
        return n;
    }

    //Boolean

    bool operator==(v3 other){
        return x == other.x && y == other.y && z == other.z;
    }
    bool operator!=(v3 other){
        return x != other.x || y != other.y || z != other.z;
    }
};
