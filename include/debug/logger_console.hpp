#pragma once
#include <hincl.hpp>

void _log     (const char* file, const char* function, int line_number, const char* string, ...);
void _warning (const char* file, const char* function, int line_number, const char* string, ...);
void _error   (const char* file, const char* function, int line_number, const char* string, ...);

void _assert  (const char* condition, const char* file, const char* function, int line_number, const char* string, ...);

typedef struct ILogger ILogger;
ILogger* get_console_logger();
