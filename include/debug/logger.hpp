#pragma once
#include <hincl.hpp>

typedef struct ILogger{
    void (*log)     (const char* file, const char* function, int line_number, const char* format, ...);
    void (*warning) (const char* file, const char* function, int line_number, const char* format, ...);
    void (*error)   (const char* file, const char* function, int line_number, const char* format, ...);
    void (*assert)  (const char* condition, const char* file, const char* function, int line_number, const char* format, ...);
} ILogger;


