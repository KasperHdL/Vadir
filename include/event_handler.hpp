#pragma once
#include <hincl.hpp>

#include <events.hpp>

typedef void (*EventFunction)(void* args);


namespace EventHandler{
    void initialize(int capacity);

    void enable_logging();
    void disable_logging();

#if DEBUG
    void _subscribe(Event event, EventFunction function, const char* debug_event_name, const char* debug_func_name);
    void _unsubscribe(Event event, EventFunction function, const char* debug_event_name, const char* debug_func_name);
    void _trigger(Event event, void* arguments, const char* debug_event_name);
#else

    void _subscribe(Event event, EventFunction function);
    void _unsubscribe(Event event, EventFunction function);
    void _trigger(Event event, void* arguments);
#endif

};

#if DEBUG
#define EVENT_SUBSCRIBE(event, function) EventHandler::_subscribe(event, function, #event, #function)
#define EVENT_UNSUBSCRIBE(event, function) EventHandler::_unsubscribe(event, function, #event, #function)
#define EVENT_TRIGGER(event, args) EventHandler::_trigger(event, args, #event)
#else
#define EVENT_SUBSCRIBE(event, function) EventHandler::_subscribe(event, function)
#define EVENT_UNSUBSCRIBE(event, function) EventHandler::_unsubscribe(event, function)
#define EVENT_TRIGGER(event, args) EventHandler::_trigger(event, args)
#endif
