#pragma once
#include <hincl.hpp>

class ISystem
{
  public:
    virtual u64 get_type() = 0;
};

//typedef void (*SystemEvent)(ISystem* system, void* args);

//HELPER MACRO

#define MAKE_GET_SET_REF_FUNCTIONS(system, component_type, data_struct_name, data_type, data_name) \
    data_type system::get_##data_name(component_type component)                                    \
    {                                                                                              \
        return data_struct_name.data_name[component.id];                                           \
    }                                                                                              \
    void system::set_##data_name(component_type component, data_type data_name)                    \
    {                                                                                              \
        data_struct_name.data_name[component.id] = data_name;                                      \
    }                                                                                              \
    data_type& system::ref_##data_name(component_type component)                                   \
    {                                                                                              \
        return data_struct_name.data_name[component.id];                                           \
    }
