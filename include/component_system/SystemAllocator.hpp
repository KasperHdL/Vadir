#pragma once
#include <hincl.hpp>

class SystemAllocator
{
  public:
    ///Requires pointer adresses to be set beforehand, throguh set_pointer_address.
    void allocate(uint capacity, uint count, bool init_zeroed = false);
    void deallocate();

    ///Set the length of the pointer head array, so that there is room for the amount of pointer addresses that will be send through set_pointer_head
    void set_num_pointer_addresses(uint num_pointer_addresses);
    ///Send the address of the pointer, and the size of the unit to be placed there, from this allocate will figure out how big the allocation should be.
    void set_pointer_address(void* pointer_address, uint unit_size);

  private:
    void free_buffers();

    void* buffer      = nullptr;
    uint  buffer_size = 0;

    void** pointer_addresses;
    uint*  pointer_addresses_unit_size;

    uint pointer_addresses_length;
    uint pointer_addresses_count;
};
