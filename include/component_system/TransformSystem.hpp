#pragma once
#include <component_system/ISystem.hpp>
#include <hincl.hpp>
#include <math/vector.hpp>

struct TransformComp
{
    u64 id;
};


class TransformSystem : public ISystem
{
  public:
    u64 get_type() override;

    void allocate(u64 capacity);

    TransformComp create(u64 entity);
    TransformComp lookup(u64 entity);

    void event_func(void* args);

    //Get Refs Sets

    v3   get_position(TransformComp component);
    v3&  ref_position(TransformComp component);
    void set_position(TransformComp component, v3 position);

    v3   get_rotation(TransformComp component);
    v3&  ref_rotation(TransformComp component);
    void set_rotation(TransformComp component, v3 rotation);

    float  get_scale(TransformComp component);
    float& ref_scale(TransformComp component);
    void   set_scale(TransformComp component, float scale);
};
