#pragma once
#include <hincl.hpp>

enum class Event{
    Test,
    SomeOtherTest,

    Count
};

struct EventArg_Test{
    int some_args;
    int other_args;
};
