#pragma once
#include <hincl.hpp>

class ISystem;
namespace SystemLocator
{
void allocate(u64 capacity);
void deallocate();

bool register_system(ISystem* system);

ISystem* get_system(u64 hash);
}; // namespace SystemLocator
