#include <cincl.hpp>
#include <hincl.hpp>

#include <stdio.h>

#include <event_handler.hpp>
//#include <memory/allocator.hpp>

#include <test.hpp>

#include <SystemLocator.hpp>

#include <component_system/TransformSystem.hpp>

TransformSystem transform_system;

int main()
{
    Global::init_with_defaults();
    EventHandler::initialize(8);

    SystemLocator::allocate(64);
    transform_system.allocate(64);
    SystemLocator::register_system(&transform_system);

    transform_system.create(1);
    transform_system.create(3);
    transform_system.create(2);

    Test::run(true);


    /*
    TransformComponent c = transform_system.lookup(3);
    LOGF("id: %i (%f %f %f)", c.id,
            transform_system.data.pos[c.id].x,
            transform_system.data.pos[c.id].y,
            transform_system.data.pos[c.id].z);

    transform_system.ref_position(c) += {1,1,1};

    LOGF("id: %i (%f %f %f)", c.id,
            transform_system.data.pos[c.id].x,
            transform_system.data.pos[c.id].y,
            transform_system.data.pos[c.id].z);

    transform_system.ref_position(c) -= {10,10,10};

    LOGF("id: %i (%f %f %f)", c.id,
            transform_system.data.pos[c.id].x,
            transform_system.data.pos[c.id].y,
            transform_system.data.pos[c.id].z);
    */

    return 0;
}

/*
 * Thinking about ECS DOD API
 */


/*

    ITransform* system = (ITransform*) verse.get_system_and_select(SH("Transform"), entity);
    system->add_position(5,0,0);

    TransformSystem* system = (ITransform*) SystemLocator::get(SH("Transform"));
    TransformComponent comp = system->get(entity);
    system.add_position(comp, {5,0,0});

    system.ref_position(comp) += {5,0,0};



    ITransform* system = (ITransform*) SystemLocator::get(SH("Transform"));
    system->select(entity);
    system->add_position(5,0,0);

    ITransform* system = (ITransform*) verse.get_system(SH("Transform"))->select(entity);
    system->add_position(5,0,0);

    ITransform* system = (ITransform*) verse.get_system(SH("Transform"))->select(entity);
    system->add_position(5,0,0);

    ITransform* system = (ITransform*) verse.get_system(SH("Transform"))->select(entity);
    vec3 pos = system->get_position();
    system->set_position(pos + (5,0,0);

    ((ITransform*) verse.get_system_and_select(SH("Transform"),entity))->add_position(5,0,0);

    ITransform* system = (ITransform*) verse.get_system(SH("Transform")); //pointer could be stored

    TransformComponent t_comp = system->lookup(entity);
    system.data.positions[t_comp.id] += (5,0,0);//dirtying would have to work a different way then

*/
