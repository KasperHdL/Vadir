#include <global.hpp>

#include <debug/logger.hpp>
#include <debug/logger_console.hpp>

#include <memory/ring_allocator.hpp>

namespace Global
{

ILogger*    logger;
IAllocator* allocator;
IAllocator* temp_allocator;

//forward declared private functions
IAllocator* get_allocator(u64 hash);

void init_with_defaults()
{
    logger = get_console_logger();

    temp_allocator = get_ring_allocator();
    //allocator = get_stack_allocator(1024);
}
void init(ILogger* logger, IAllocator* allocator, IAllocator* temp_allocator)
{
    Global::logger         = logger;
    Global::allocator      = allocator;
    Global::temp_allocator = temp_allocator;
}

void set_logger(u64 logger_hash)
{
    switch (logger_hash)
    {
        case STATIC_HASH("console_logger", 3740961542):
            logger = get_console_logger();
            break;
        case STATIC_HASH("file_logger", 4113197746):
            logger = get_console_logger();
            WARNING("file logging is not implemented yet");
            break;
    }
}


void set_allocator(u64 hash)
{
    allocator = get_allocator(hash);
}
void set_temp_allocator(u64 hash)
{
    temp_allocator = get_allocator(hash);
}

IAllocator* get_allocator(u64 hash)
{
    switch (hash)
    {
        case STATIC_HASH("ring_allocator", 1288434153):
            return get_ring_allocator();
            break;
        default:
            return 0;
            break;
            /*
        case STATIC_HASH("stack_allocator", 3227972102):
            return get_stack_allocator();
        break;
        */
    }
}
} // namespace Global
