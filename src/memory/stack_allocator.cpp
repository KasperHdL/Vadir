#include <memory/stack_allocator.hpp>
#include <cincl.hpp>

#include <stdio.h>
#include <stdlib.h>
#include <memory/allocator.hpp>

#define RING_BUFFER_SIZE 1024

typedef struct Header{
    u32 size;
} Header;

typedef struct StackAllocator{
    IAllocator vtable;
    char* _buffer;

    char* _head;
    char* _end;

    u32 _size;
} StackAllocator;


IAllocator* get_stack_allocator(u32 size){
    StackAllocator* allocator = (StackAllocator*) malloc(sizeof(StackAllocator));

    allocator->_buffer = (char*) calloc(1, size);
    allocator->_size = size;

    allocator->_head = allocator->_buffer;
    allocator->_end  = allocator->_buffer + RING_BUFFER_SIZE;

    allocator->vtable = (IAllocator){
        .allocate        = &_stack_allocate,
        .deallocate      = &_stack_deallocate,
        .allocated_size  = &_stack_allocated_size,
        .total_allocated = &_stack_total_allocated
    };

    return (IAllocator*)allocator;
}

void* _stack_allocate(IAllocator* allocator, u32 size){
    StackAllocator* stack = (StackAllocator*) allocator;

    char* p = stack->_head;
    u32 size_with_header = size + sizeof(Header);
    if(p + size_with_header > stack->_end){
        ERROR("Allocation would exceed capacity");
        return NULL;
    }

    char* p_header = p + size;
    Header* header = (Header*) p_header;
    header->size = size;

    stack->_head = p + size_with_header;

    return (void*) p;
}

void  _stack_deallocate(IAllocator* allocator, void* pointer){
    StackAllocator* stack = (StackAllocator*) allocator;

    char* last_header = stack->_head - sizeof(Header);
    if(last_header < stack->_buffer){
        ERROR("Trying to free something that has not been allocated");
        return;
    }

    Header* header = (Header*)last_header;

    u32 size = header->size;
    char* p = (char*)header - size;

    stack->_head = p;
}

u32 _stack_allocated_size(IAllocator* allocator, void* pointer){
    StackAllocator* stack = (StackAllocator*) allocator;

    char* last_header = stack->_head - sizeof(Header);
    if(last_header < stack->_buffer){
        ERROR("Has not allocated anything yet, therefor cannot return size of last allocation");
        return 0;
    }
    Header* header = (Header*)last_header;

    return header->size;
}

u32 _stack_total_allocated(IAllocator* allocator){
    StackAllocator* stack = (StackAllocator*) allocator;
    return stack->_size;
}
