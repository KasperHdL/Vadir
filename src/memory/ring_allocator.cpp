#include <memory/ring_allocator.hpp>
#include <cincl.hpp>

#include <stdio.h>
#include <stdlib.h>
#include <memory/allocator.hpp>

typedef struct RingAllocator{
    IAllocator vtable;
    char* _buffer;

    char* _head;
    char* _end;

} RingAllocator;

IAllocator* get_ring_allocator(){
    RingAllocator* ring = (RingAllocator*) malloc(sizeof(RingAllocator));

    ring->_buffer = (char*) calloc(1, RING_BUFFER_SIZE);

    ring->_head = ring->_buffer;
    ring->_end  = ring->_buffer + RING_BUFFER_SIZE;

    ring->vtable = (IAllocator){
        .allocate        = &_ring_allocate,
        .deallocate      = &_ring_deallocate,
        .allocated_size  = &_ring_allocated_size,
        .total_allocated = &_ring_total_allocated
    };

    return (IAllocator*)ring;
}

void* _ring_allocate(IAllocator* allocator, u32 size){
    RingAllocator* ring = (RingAllocator*) allocator;

    char* p = ring->_head;
    if(p + size > ring->_end){
        p = ring->_buffer;
    }

    ring->_head = p + size;

    return (void*) p;
}

void _ring_deallocate(IAllocator* allocator, void* pointer){
    RingAllocator* ring = (RingAllocator*) allocator;

    ring->_head = ring->_buffer;
}

u32 _ring_allocated_size(IAllocator* allocator, void* pointer){
    return 0;
}

u32 _ring_total_allocated(IAllocator* allocator){
    return RING_BUFFER_SIZE;
}
