#include <memory/array.hpp>

#include <cincl.hpp>
#include <memory.h>
#include <stdlib.h>


template<class T>
void Array<T>::allocate(uint capacity)
{
    buffer         = (T*)malloc(capacity * sizeof(T));
    this->capacity = capacity;
    count          = 0;
}

template<class T>
void Array<T>::reallocate(uint capacity)
{
#if DEBUG
    ASSERTF(count < capacity, "Cannot allocate a capacity(%i) that is less than the current array count(%i)", capacity, count);
#endif

    buffer         = (T*)realloc(buffer, capacity * sizeof(T));
    this->capacity = capacity;
}

template<class T>
void Array<T>::deallocate()
{
    free(buffer);
    capacity = 0;
    count    = 0;
}

template<class T>
void Array<T>::add(T value)
{
    if (count >= capacity)
    {
        reallocate(capacity * 2);
    }

    buffer[count++] = value;
}

template<class T>
void Array<T>::set(uint index, T value)
{
    if (index >= capacity)
    {
        int multiplier = ((float)index / capacity) + 1;
        reallocate(multiplier * capacity);
    }

    if (index > count)
        count = index + 1;

    buffer[index] = value;
}

template<class T>
T Array<T>::get_copy(uint index)
{
    return buffer[index];
}

template<class T>
T* Array<T>::get_pointer(uint index)
{
    return &buffer[index];
}

template<class T>
T& Array<T>::operator[](uint index)
{
#if DEBUG
    if (index < 0 || index >= capacity)
    {
        ERRORF("Index(%i) out of range(%i), returning buffer[0]", index, capacity);
        return buffer[0];
    }
#endif

    return buffer[index];
}

template<typename T>
Array<T>& Array<T>::operator=(const Array<T>& other)
{
    if (other.capacity > capacity)
        reallocate(other.capacity);

    memcpy(buffer, other.buffer, sizeof(T) * other.capacity);
    count = other.count;

    return *this;
}


template class Array<int>;
template class Array<float>;
