#include <event_handler.hpp>
#include <cincl.hpp>


#include <stdio.h>
#include <stdlib.h>


#define FUNCTION_NAME_LENGTH 256

EventFunction* functions;
int* num_subscribers;

#if DEBUG
    const char** function_names;
#endif

int capacity_per_event;
bool is_logging;


namespace EventHandler{

    void initialize(int capacity){
        capacity_per_event = capacity;

        functions = (EventFunction*) calloc((int)Event::Count, sizeof(EventFunction) * capacity_per_event);
        num_subscribers = (int*) calloc((int)Event::Count, sizeof(int));


#if DEBUG
        is_logging = true;

        function_names = (const char**) calloc((int)Event::Count * capacity_per_event * FUNCTION_NAME_LENGTH, sizeof(const char));
#else
        is_logging = false;
#endif

    }

    void enable_logging(){
        is_logging = true;
    }

    void disable_logging(){
        is_logging = false;
    }

#if DEBUG
    void _subscribe(Event event, EventFunction function, const char* debug_event_name, const char* debug_func_name){
        if(num_subscribers[(int) event] > capacity_per_event){
            ERRORF("Cannot subscribe to Event(%i)", event);
            return;
        }

        if(is_logging)
            printf("[Event:Subscribe - %s] %s\n", debug_event_name, debug_func_name);

        int num = num_subscribers[(int)event]++;
        int index = (int) event + num;
        functions[index] = function;

        function_names[index] = debug_func_name;
    }

    void _unsubscribe(Event event, EventFunction function, const char* debug_event_name, const char* debug_func_name){
        if(num_subscribers[(int) event] == 0){
            ERRORF("Cannot unsubscribe from Event(%i) no one is subscribing", debug_event_name);
            return;
        }

        if(is_logging)
            printf("[Event:Unsubscribe - %s] %s\n", debug_event_name, debug_func_name);
        bool found = false;
        int num_subs = num_subscribers[(int)event];

        for(int i = 0; i < num_subs; i++){
            int index = (int)event + i;

            if(!found && functions[index] == function){
                found = true;
            }

            if(found && i + 1 < num_subs){
                functions[index] = functions[index + 1];
                function_names[index] = function_names[index + 1];
            }

        }

        functions[(int)event + num_subs] = NULL;
        num_subscribers[(int)event]--;

    }

    void _trigger(Event event, void* arguments, const char* debug_event_name){
        if(is_logging)
            printf("[Event:Trigger - %s] calling %i subscribers: \n", debug_event_name, num_subscribers[(int)event]);

        for(int i = 0; i < num_subscribers[(int)event]; i++){
            int index = (int)event + i;
            if(is_logging)
                printf("\t%i - %s\n", i, function_names[index]);

            functions[index](arguments);
        }
    }
#else
    void _trigger(Event event, void* arguments){
        for(int i = 0; i < num_subscribers[(int)event]; i++){
            int index = (int)event + i;

            functions[index](arguments);
        }
    }

    void _subscribe(Event event, EventFunction function){
        if(num_subscribers[(int) event] > capacity_per_event){
            return;
        }

        int num = num_subscribers[(int)event]++;
        int index = (int) event + num;
        functions[index] = function;

    }

    void _unsubscribe(Event event, EventFunction function){
        if(num_subscribers[(int) event] == 0){
            return;
        }

        bool found = false;
        int num_subs = num_subscribers[(int)event];


        for(int i = 0; i < num_subs; i++){
            int index = (int)event + i;

            if(!found && functions[index] == function){
                found = true;
            }

            if(found && i + 1 < num_subs){
                functions[index] = functions[index + 1];
            }
        }

        functions[(int)event + num_subs] = NULL;
        num_subscribers[(int)event]--;
    }

#endif

};
