#include <component_system/SystemAllocator.hpp>

#include <cincl.hpp>
#include <memory.h>
#include <stdlib.h>

void SystemAllocator::set_num_pointer_addresses(uint num_pointer_addresses)
{
    pointer_addresses           = (void**)malloc(sizeof(void*) * num_pointer_addresses);
    pointer_addresses_unit_size = (uint*)malloc(sizeof(uint) * num_pointer_addresses);

    pointer_addresses_length = num_pointer_addresses;
    pointer_addresses_count  = 0;
}
void SystemAllocator::allocate(uint capacity, uint count, bool init_zeroed)
{

    if (pointer_addresses_count == 0)
    {
        ERROR("pointer addresses must be set first, use set_pointer_address");
        return;
    }

    if (pointer_addresses_count != pointer_addresses_length)
    {
        ERRORF("The number of pointer addresses given does not match the amount declared initially, count(%i) length(%i)", pointer_addresses_count, pointer_addresses_length);
        return;
    }

    uint size = 0;
    for (int i = 0; i < pointer_addresses_count; i++)
    {
        size += pointer_addresses_unit_size[i] * capacity;
    }

    ASSERTF(size >= buffer_size, "Allocation size(%i) must be greater than the existing allocation buffer_size(%i)", size, buffer_size);

    void* new_buffer;
    if (init_zeroed)
    {
        new_buffer = calloc(1, size);
    } else
    {
        new_buffer = malloc(size);
    }

    void* head = new_buffer;

    //set pointer addresses and copy old data
    for (int i = 0; i < pointer_addresses_count; i++)
    {
        //////////////////////////////
        //IF YOU SEGFAULT AROUND HERE, you have probably forgot to send the address of the pointer when setting pointer addresses, you should use (&entity) not (entity)
        //////////////////////////////

        //copy old data
        if (count > 0)
        {
            memcpy(head, *(void**)pointer_addresses[i], pointer_addresses_unit_size[i] * count);
        }
        //update pointer head
        *(void**)pointer_addresses[i] = head;

        //increment head pointer
        head = (char*)head + pointer_addresses_unit_size[i] * capacity;
    }

    free_buffers();
    buffer      = new_buffer;
    buffer_size = size;
}


void SystemAllocator::deallocate()
{
    free_buffers();
}
void SystemAllocator::free_buffers()
{
    free(pointer_addresses);
    pointer_addresses = nullptr;

    free(pointer_addresses_unit_size);
    pointer_addresses_unit_size = nullptr;

    free(buffer);
    buffer      = nullptr;
    buffer_size = 0;
}


void SystemAllocator::set_pointer_address(void* pointer_address, uint unit_size)
{
    if (pointer_addresses_count >= pointer_addresses_length ||
        unit_size == 0)
        goto error;

    pointer_addresses[pointer_addresses_count]           = pointer_address;
    pointer_addresses_unit_size[pointer_addresses_count] = unit_size;

    pointer_addresses_count++;

    return;
error:

    if (pointer_addresses_count >= pointer_addresses_length)
    {
        ERRORF("More pointer addresses set than defined in set_num_pointer_addresses(), length set %i", pointer_addresses_length);
        return;
    }
    if (unit_size == 0)
    {
        ERROR("cannot set unit size to 0");
        return;
    }
}
