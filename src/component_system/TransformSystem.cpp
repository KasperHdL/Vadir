#include <component_system/TransformSystem.hpp>

#include <cincl.hpp>
#include <component_system/SystemAllocator.hpp>
#include <event_handler.hpp>

namespace
{
struct Data
{
    u64    count    = 0;
    u64    capacity = 0;
    u64*   id;
    v3*    position;
    v3*    rotation;
    float* scale;
};

Data            data;
SystemAllocator system_allocator;
} // namespace

MAKE_GET_SET_REF_FUNCTIONS(TransformSystem, TransformComp, data, v3, position)
MAKE_GET_SET_REF_FUNCTIONS(TransformSystem, TransformComp, data, v3, rotation)
MAKE_GET_SET_REF_FUNCTIONS(TransformSystem, TransformComp, data, float, scale)

u64 TransformSystem::get_type()
{
    return STATIC_HASH("TransformSystem", 10703671074388600221U);
}

void TransformSystem::allocate(u64 capacity)
{
    ASSERT(capacity > data.count)

    data.capacity = capacity + 1;

    system_allocator.set_num_pointer_addresses(4);

    //set pointer addresses
    system_allocator.set_pointer_address(&data.id, sizeof(u64));
    system_allocator.set_pointer_address(&data.position, sizeof(v3));
    system_allocator.set_pointer_address(&data.rotation, sizeof(v3));
    system_allocator.set_pointer_address(&data.scale, sizeof(float));

    //allocate and set the pointer addresses
    system_allocator.allocate(data.capacity, data.count, true);

    //EventFunction test = &ISystem::event_func;
    //EVENT_SUBSCRIBE(Event::Test, test);
}

void TransformSystem::event_func(void* args)
{
}

TransformComp TransformSystem::create(u64 entity)
{
    if (data.capacity == 0)
        ERROR("Cannot create component before System has been allocated");

    u64 id = data.count++;

    data.id[id] = entity;

    data.position[id] = {0, 0, 0};
    data.rotation[id] = {0, 0, 0};
    data.scale[id]    = 0;

    return {id};
}

TransformComp TransformSystem::lookup(u64 entity)
{
    u64 id = 0;

    for (int i = 1; i < data.count; i++)
    {
        if (data.id[i] == entity)
        {
            id = i;
            break;
        }
    }

    return {id};
}
