#include <cincl.hpp>
#include <test.hpp>

#include <memory/array.hpp>

#include <SystemLocator.hpp>
#include <component_system/TransformSystem.hpp>

namespace Test
{

void array(bool verbose)
{
    Array<int>   arr;
    Array<float> arrf;

    if (verbose)
    {
        LOGF("(count: %i, capacity: %i, buffer: %p)", arr.count, arr.capacity, arr.buffer);
        LOGF("(count: %i, capacity: %i, buffer: %p)", arrf.count, arrf.capacity, arrf.buffer);
    }

    arr.allocate(22);
    arrf.allocate(32);

    if (verbose)
    {
        LOGF("(count: %i, capacity: %i, buffer: %p)", arr.count, arr.capacity, arr.buffer);
        LOGF("(count: %i, capacity: %i, buffer: %p)", arrf.count, arrf.capacity, arrf.buffer);
    }

    arr.set(0, 1025);
    arrf.set(0, 1025);

    for (int i = 0; i < 512; i++)
    {
        arr.set(i, i);
        arrf.add(i);
    }

    for (int i = 0; i < arr.count; i++)
    {
        arr[i] += 10000;
        if (verbose)
        {
            LOGF("%i: %i", i, arr[i]);
        }
    }
    for (int i = 0; i < arrf.count; i++)
    {
        arrf[i] += 10000;
        if (verbose)
        {
            LOGF("%i: %f", i, arrf[i]);
        }
    }

    if (verbose)
    {
        LOGF("(count: %i, capacity: %i, buffer: %p)", arr.count, arr.capacity, arr.buffer);
        LOGF("(count: %i, capacity: %i, buffer: %p)", arrf.count, arrf.capacity, arrf.buffer);
    }

    arr.deallocate();
    arrf.deallocate();
}

void system_locator(bool verbose)
{
    bool             correct   = true;
    TransformSystem* transform = (TransformSystem*)SystemLocator::get_system(STATIC_HASH("TransformSystem", 10703671074388600221U));

    v3  t      = {5, 1, 10};
    int entity = 1;

    TransformComp comp = transform->lookup(entity);
    transform->ref_position(comp) += t;

    v3 v = transform->get_position(comp);
    if (verbose)
    {
        LOGF("transform(%i).pos = (%f, %f, %f)", entity, v.x, v.y, v.z);
    }
    if (v != t)
    {
        ERRORF("Failed, expected (%f, %f, %f) received (%f, %f, %f)", t.x, t.y, t.z, v.x, v.y, v.z);
    }

    v3 t2 = {1, 1, 11};
    transform->ref_position(comp) += t2;

    v = transform->get_position(comp);
    if (verbose)
    {
        LOGF("transform(%i).pos = (%f, %f, %f)", entity, v.x, v.y, v.z);
    }
    if (v != t + t2)
    {
        v3 tt2 = t + t2;
        ERRORF("Failed, expected (%f, %f, %f) received (%f, %f, %f)", tt2.x, tt2.y, tt2.z, v.x, v.y, v.z);
    }
}

void run(bool verbose)
{
    array(verbose);
    system_locator(verbose);

    LOG("Tests has been run successfully");
}
} // namespace Test
