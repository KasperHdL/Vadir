#include <SystemLocator.hpp>
#include <cincl.hpp>

#include <stdlib.h>

#include <component_system/ISystem.hpp>

namespace SystemLocator
{

struct Data
{
    u64   capacity = 0;
    u64   count    = 0;
    void* buffer;

    u64*      type_hash;
    ISystem** system;
};

Data data;

ISystem* get_system_no_error(u64 hash);

void allocate(u64 capacity)
{
    u64 size = capacity * (sizeof(u64) + sizeof(ISystem*));

    data.buffer = malloc(size);

    data.type_hash = (u64*)data.buffer;
    data.system    = (ISystem**)data.type_hash + capacity;

    data.capacity = capacity;

    if (data.count == 0)
    {
        data.count = 1;

        data.type_hash[0] = 0;
        data.system[0]    = nullptr;
    }
}

void deallocate()
{
    data.count    = 0;
    data.capacity = 0;
    free(data.buffer);
    data.type_hash = nullptr;
    data.system    = nullptr;
}

bool register_system(ISystem* system)
{
    u64 hash = system->get_type();

    ISystem* existing_system = get_system_no_error(hash);
    if (existing_system != nullptr)
    {
        bool equal = system == existing_system;
        if (equal)
        {
            ERRORF("System is already registered with the same hash(%u)", hash);
        } else
        {
            ERRORF("Another System with hash(%u) already exists", hash);
        }
        return false;
    }

    u64 id             = data.count++;
    data.type_hash[id] = hash;
    data.system[id]    = system;

    return true;
}

ISystem* get_system(u64 hash)
{
    int id = 0;
    for (int i = 1; i < data.count; i++)
    {
        if (data.type_hash[i] == hash)
        {
            id = i;
            break;
        }
    }
    if (id == 0)
    {
        ERRORF("No System with the hash(%u), check your spelling", hash);
        return nullptr;
    }

    return data.system[id];
}

ISystem* get_system_no_error(u64 hash)
{
    int id = 0;
    for (int i = 1; i < data.count; i++)
    {
        if (data.type_hash[i] == hash)
        {
            id = i;
            break;
        }
    }
    return data.system[id];
}
} // namespace SystemLocator
