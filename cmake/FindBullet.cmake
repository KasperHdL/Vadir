message("-- BULLET")

if(WIN32)
	set(BULLET_DYNAMICS_LIBRARY 
		${PLATFORM_PATHS}/bullet3/${PLATFORM_ARCH}/BulletDynamics.lib
		)
elseif(UNIX)
	set(BULLET_DYNAMICS_LIBRARY 
		${PLATFORM_PATHS}/bullet3/libBulletDynamics.a
		)
endif()

if(WIN32)
	set(BULLET_COLLISION_LIBRARY 
		${PLATFORM_PATHS}/bullet3/${PLATFORM_ARCH}/BulletCollision.lib
		)
elseif(UNIX)
	set(BULLET_COLLISION_LIBRARY 
		${PLATFORM_PATHS}/bullet3/libBulletCollision.a
		)
endif()

if(WIN32)
	set(BULLET_LINEAR_MATH_LIBRARY 
		${PLATFORM_PATHS}/bullet3/${PLATFORM_ARCH}/LinearMath.lib
		)
elseif(UNIX)
    set(BULLET_LINEAR_MATH_LIBRARY 
		${PLATFORM_PATHS}/bullet3/libLinearMath.a
		)
endif()

if(WIN32)
    set(BULLET_BUSS_IK_LIBRARY 
		${PLATFORM_PATHS}/bullet3/${PLATFORM_ARCH}/BussIK.lib
		)
elseif(UNIX)
    set(BULLET_BUSS_IK_LIBRARY 
		${PLATFORM_PATHS}/bullet3/libBussIK.a
		)
endif()

SET(BULLET_LIBRARY 
    ${BULLET_DYNAMICS_LIBRARY} 
    ${BULLET_COLLISION_LIBRARY} 
    ${BULLET_LINEAR_MATH_LIBRARY} 
    ${BULLET_BUSS_IK_LIBRARY}
    CACHE STRING "All the Bullet Libraries"
    )
