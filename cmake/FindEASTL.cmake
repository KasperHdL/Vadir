message("-- EASTL")

if(WIN32)
	set(EASTL_LIBRARY 
		${PLATFORM_PATHS}/eastl/${PLATFORM_ARCH}/EASTL.lib
		CACHE STRING "Where to find EASTL.lib"
		)
elseif(UNIX)
	set(EASTL_LIBRARY 
		${PLATFORM_PATHS}/eastl/libEASTL.a
		CACHE STRING "Where to find libEASTL.a"
		)
endif()

