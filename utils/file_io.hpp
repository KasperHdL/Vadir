#pragma once
#include <stdio.h>


char* read_file_as_array(char* file_name, long* file_length)
{
    FILE* file = fopen(file_name, "r");
    if (!file) return NULL;

    fseek(file, 0L, SEEK_END);
    long file_size = ftell(file);
    if (file_size == 0)
    {
        fclose(file);
        printf("[file_io::Warning] file is empty(%s)\n", file_name);
        *file_length = 0;
        return NULL;
    }

    rewind(file);

    *file_length = file_size + 1;

    char* buffer = (char*)malloc(file_size + 1);
    if (!buffer)
    {
        fclose(file);
        printf("[file_io::Error] could not allocate buffer of size %i\n", file_size + 1);
        *file_length = 1;
        return NULL;
    }

    if (fread(buffer, file_size, 1, file) != 1)
    {
        fclose(file);
        free(buffer);
        printf("[file_io::Error] could not read file(%s) into buffer\n", file_name);
        *file_length = 1;
        return NULL;
    }

    fclose(file);
    buffer[file_size] = '\0';
    return buffer;
}

bool write_file_from_array(char* file_name, char* buffer, int buffer_size)
{
    FILE* file = fopen(file_name, "w");
    if (!file) return false;

    if (fwrite(buffer, buffer_size, 1, file) != 1)
    {
        fclose(file);
        printf("[file_io::Error] could not write file\n");
        return false;
    }

    fclose(file);

    return true;
}
